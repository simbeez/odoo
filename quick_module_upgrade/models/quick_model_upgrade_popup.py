from odoo import api, fields, models,_
from odoo import exceptions
from atom import Date
from datetime import date, datetime
from odoo.exceptions import UserError

class QuickModelUpgrade(models.TransientModel):
    _name='quick.model.upgrade' 
    
    
    @api.model
    def get_recent_modules(self):
        return self.env['ir.module.module'].search([('last_update_module_date','!=',False)],order="last_update_module_date desc",limit=5).ids
    
    module_ids = fields.Many2many('ir.module.module','quick_modele_upgrade_ir_mudule_rel','wizard_id','module_id',string='Modules') 
    recent_module_ids = fields.Many2many('ir.module.module','quick_modele_upgrade_ir_mudule_recent_rel','wizard_id','module_id',string='Recent Upgraded Modules',default=get_recent_modules)
    
    
    
    
    @api.multi
    def upgrade_button(self):
        if not self.module_ids:
            raise UserError(_('No Modules Selected for upgrade!!'))
        else:    
            for module_obj in self.module_ids:
                module_obj.button_immediate_upgrade()
                   
        return  
    
    