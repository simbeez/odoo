from odoo import api, fields, models
from datetime import date, datetime
from odoo.addons.base.module import module

class IRMOdule(models.Model):
    _inherit='ir.module.module'
    
    last_update_module_date=fields.Datetime('Last Update Date')
    
    @module.assert_log_admin_access
    @api.multi
    def button_immediate_upgrade(self):
        res = super(IRMOdule,self).button_immediate_upgrade()
        self.write({'last_update_module_date' :datetime.strftime(datetime.now(),'%Y-%m-%d %H:%M:%S')})
        return res