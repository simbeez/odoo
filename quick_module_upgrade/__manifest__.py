# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Quick Module Upgrade',
    'version' : '1.0',
    'summary': 'Quick Module Upgrade',
    'license' : 'LGPL-3',
    'support':'info@simbeez.com',
    'description': """
    """,
    'website': 'https://simbeez.com/',
    'depends' : ['base'],
    'data': [
              'view/quick_models_upgrade_popup_view.xml',
             ],
    
    'installable': True,
    'application': False,
    'auto_install': False,
}
